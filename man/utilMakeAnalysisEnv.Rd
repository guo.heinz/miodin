% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utilsDataProcessing.R
\name{utilMakeAnalysisEnv}
\alias{utilMakeAnalysisEnv}
\title{utilMakeAnalysisEnv}
\usage{
utilMakeAnalysisEnv(
  desc = "",
  moduleName = "",
  utilName = "",
  utilPackage = "",
  utilVersion = "",
  fileName = "",
  resultName = "",
  interpretation = "",
  ...
)
}
\arguments{
\item{desc}{Description of what the processing function does.}

\item{moduleName}{Name of the module calling the processing function.}

\item{utilName}{Name of the utility analysis function.}

\item{utilPackage}{Package in which the utility function is defined.}

\item{utilVersion}{Version of the package.}

\item{fileName}{Name of result file.}

\item{resultName}{Name of specific result in the file.}

\item{intepretation}{How to interpret the result.s}
}
\value{
An \code{environment} used for data analysis.
}
\description{
Creates and returns an \code{environment} for data analysis.
}
\details{
This is a support function for creating a generic data analysis
environment, used to store the documentation of an analysis step
performed by a utility function.
}
\author{
Benjamin Ulfenborg
}
