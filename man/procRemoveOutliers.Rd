% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/procsMicroarray.R
\name{procRemoveOutliers}
\alias{procRemoveOutliers}
\title{procRemoveOutliers}
\usage{
procRemoveOutliers(removeOutliersIQR, samplePheno, contrastName)
}
\arguments{
\item{removeOutliersIQR}{removeOutliersIQR IQR multiplier for
detection of outlier values.}

\item{samplePheno}{Data frame containing the sample group.}

\item{contrastName}{Name of study contrast.}
}
\value{
A data processing environment.
}
\description{
Processing function to replace outlier values with NA.
}
\details{
Values in the data are considered outliers if they are more than 1.5 times
the interquartile range (IQR * 1.5) away from the median of a variable
(e.g. gene or protein).
}
\author{
Benjamin Ulfenborg
}
