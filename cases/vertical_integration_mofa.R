#Load miodin
library(miodin)

#Create project
mp <- MiodinProject(name = "VerticalIntegration",
                    author = "TheAuthor",
                    path = path.expand("~/miodin_projects"))

#Study design
pheno <- read.table(system.file("extdata/tcga_esca_pheno.txt",
                                package = "miodindata"), header = TRUE)
sampleTable <- data.frame(
  SampleName = pheno$Sample,
  SamplingPoint = rep("sp1", nrow(pheno)),
  VitalStatus = ifelse(pheno$VitalStatus == 1, "Alive", "Diseased")
)
assayTableRNA <- data.frame(
  SampleName = pheno$Sample,
  DataFile = system.file("extdata/tcga_esca_rnaseq.txt",
                         package = "miodindata"),
  DataColumn = pheno$Sample
)
assayTableMiRNA <- data.frame(
  SampleName = pheno$Sample,
  DataFile = system.file("extdata/tcga_esca_mirnaseq.txt",
                         package = "miodindata"),
  DataColumn = pheno$Sample
)
assayTableProtein <- data.frame(
  SampleName = pheno$Sample,
  DataFile = system.file("extdata/tcga_esca_rppa.txt",
                         package = "miodindata"),
  DataColumn = pheno$Sample
)
assayTableMeth <- data.frame(
  SampleName = pheno$Sample,
  DataFile = system.file("extdata/tcga_esca_meth.txt",
                         package = "miodindata"),
  DataColumn = pheno$Sample
)
ms <- studyDesignCaseControl(
  studyName = "IntegrativeESCA",
  factorName = "VitalStatus",
  contrastName = "VitalStatus",
  caseName = "Diseased",
  controlName = "Alive",
  numCase = 80,
  numControl = 44,
  sampleTable = sampleTable,
  assayTable = assayTableRNA,
  assayTableName = "RNAseq"
) +
  studyAssayTable(name = "miRNAseq", dataFrame = assayTableMiRNA) +
  studyAssayTable(name = "Protein", dataFrame = assayTableProtein) +
  studyAssayTable(name = "Meth", dataFrame = assayTableMeth)
insert(ms, mp)
rm(pheno, sampleTable, assayTableRNA, assayTableMiRNA, assayTableProtein, assayTableMeth)

#Workflow for importing and processing data
mw <- MiodinWorkflow("IntegrativeESCA")
mw <- mw +
  importProcessedData(
    name = "RNAseq imported",
    experiment = "sequencing",
    dataType = "rna",
    studyName = "IntegrativeESCA",
    assayName = "RNAseq",
    datasetName = "RNAseq",
    contrastName = "VitalStatus") %>%
  processSequencingData(
    name = "RNAseq processor",
    contrastName = "VitalStatus",
    stabilizeVariance = TRUE,
    stabilizeVarianceMethod = "vst") +
  importProcessedData(
    name = "miRNAseq imported",
    experiment = "sequencing",
    dataType = "rna",
    studyName = "IntegrativeESCA",
    assayName = "miRNAseq",
    datasetName = "miRNAseq",
    contrastName = "VitalStatus") %>%
  processSequencingData(
    name = "miRNAseq processor",
    contrastName = "VitalStatus",
    stabilizeVariance = TRUE,
    stabilizeVarianceMethod = "vst") +
  importProcessedData(
    name = "Protein imported",
    experiment = "microarray",
    dataType = "protein",
    studyName = "IntegrativeESCA",
    assayName = "Protein",
    datasetName = "Protein",
    contrastName = "VitalStatus") %>%
  processMicroarrayData(
    name = "Protein processor",
    contrastName = "VitalStatus") +
  importProcessedData(
    name = "Methyl imported",
    experiment = "microarray",
    dataType = "methylation",
    studyName = "IntegrativeESCA",
    assayName = "Meth",
    datasetName = "Meth",
    contrastName = "VitalStatus",
    chipName = "HumanMethylation450K",
    methylValues = "Beta") %>%
  processMicroarrayData(
    name = "Methyl processor",
    contrastName = "VitalStatus",
    filterSNPandCH = TRUE,
    filterNonCG = TRUE,
    corrCellComp = TRUE)
mw <- insert(mw, mp, overwrite = TRUE)
mw <- execute(mw)

#Integrative analysis
mw <- mw +
  integrateAssays(
    name = "Assay integrator",
    upstreamModules = c("RNAseq processor", "miRNAseq processor",
                        "Protein processor", "Methyl processor"),
    datasetName = "MultiOmicsSet") %>%
  performFactorAnalysis(
    name = "MOFA analysis",
    upstreamModule = "Assay integrator",
    analysisMethod = "mofa",
    annotColor = "VitalStatus",
    numFactors = 2
  )
mw <- execute(mw)

#Save and export
saveDataFile(mp)
export(mp, "dataset", "MultiOmicsSet")
export(mp, "analysisResult", "MultiOmicsSet")
