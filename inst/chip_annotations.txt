ChipName	Provider	Type	Package	Key	Annotation
HumanHT12	Illumina	RNA	NA	downloads/productfiles/humanht-12	humanht-12-v4-0-R2-15002873-b-wgdasl-txt.zip
HumanOmni25	Illumina	SNP	NA	downloads/productfiles/humanomni25/v1-3	infiniumomni2-5-8-v1-3-a2-manifest-file-csv.zip
HumanOmni25Exome	Illumina	SNP	NA	downloads/productfiles/humanomni2-5exome-8/v1-3	infinium-omni2-5-exome-8-v1-3-a2-manifest-file-csv.zip
HumanOmni25Quad	Illumina	SNP	NA	MyIllumina/d5578cf6-bb3b-4b4b-98d3-21edc5bcbd45	HumanOmni2.5-4v1_H.csv
HumanOmni5	Illumina	SNP	NA	downloads/productfiles/infinium-omni5-4/v1-2	infinium-omni5-4-v1-2-a2-manifest-file-csv.zip
HumanOmni5Exome	Illumina	SNP	NA	downloads/productfiles/humanomni5exomev1-3	infiniumomni5exome-4-v1-3-a2-manifest-file-csv.zip
HumanOmniExpress24	Illumina	SNP	NA	downloads/productfiles/humanomniexpress-24/v1-2	infinium-omniexpress-24-v1-2-manifest-file-csv.zip
HumanOmniExpressExome	Illumina	SNP	NA	downloads/productfiles/humanomniexpressexome/v1-6	infinium-omniexpressexome-8-v1-6-a2-manifest-file-csv-build38.zip
HumanMethylation450K	Illumina	Methyl	IlluminaHumanMethylation450kmanifest, IlluminaHumanMethylation450kanno.ilmn12.hg19	downloads/productfiles/humanmethylation450	humanmethylation450_15017482_v1-2.csv
HumanMethylationEpic	Illumina	Methyl	IlluminaHumanMethylationEPICmanifest, IlluminaHumanMethylationEPICanno.ilm10b4.hg19	downloads/productfiles/methylationEPIC	infinium-methylationepic-v-1-0-b4-manifest-file-csv.zip
HumanGene10ST	Affymetrix	RNA	pd.hugene.1.0.st.v1	HuGene-1_0-st-v1	PROBESUM Annotations, CSV
HumanGene20ST	Affymetrix	RNA	pd.hugene.2.0.st	HuGene-2_0-st-v1	PROBESUM Annotations, CSV
HumanExon10ST	Affymetrix	RNA	pd.huex.1.0.st.v2	HuEx-1_0-st-v2	PROBESUM Annotations, CSV
HumanTranscriptome20	Affymetrix	RNA	pd.hta.2.0	HTA-2_0	PROBESUM Annotations, CSV
HumanGenomeU133Plus20	Affymetrix	RNA	hgu133plus2.db	HG-U133_Plus_2	Annotations, CSV format
HumanGenomeU133A	Affymetrix	RNA	hgu133a.db	HG-U133A	Annotations, CSV format
HumanGenomeU133B	Affymetrix	RNA	hgu133b.db	HG-U133B	Annotations, CSV format
HumanGenomeU133PlusPM	Affymetrix	RNA	pd.ht.hg.u133.plus.pm	HT_HG-U133_Plus_PM	Annotations, CSV format
GenomeWideSNP5	Affymetrix	SNP	genomewidesnp5Crlmm	GenomeWideSNP_5	Annotations, CSV format
GenomeWideSNP6	Affymetrix	SNP	genomewidesnp6Crlmm	GenomeWideSNP_6	Annotations, CSV format
