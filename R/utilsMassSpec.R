#Include dependencies
#' @include utilsDataProcessing.R utilsPlotting.R procsSequencing.R
#' @include procsMicroarray.R
NULL

#' @title utilQualityMassSpecProtein
#' 
#' @description
#' Quality control of quantitative proteomics data.
#' 
#' @param miodinDataset \code{MiodinDataset} containing the assay data.
#' @param assayName Name of the assay to QC.
#' @param qcName Name of the QC report to add to the \code{MiodinDataset}.
#' @param studies List of \code{MiodinStudy} objects.
#' @param contrastName Name of study contrast to use for coloring QC plots
#' @param verbose Whether to print messages.
#' @param labelSamples Whether or not to label samples in the PCA plot.
#' 
#' @details
#' The following quality control plots are generated for proteomics data:
#' sample heatmap with Pearson correlation and sample PCA.
#' 
#' @return
#' A \code{MiodinDataset} containing the QC report.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @export
utilQualityMassSpecProtein <- function(miodinDataset, assayName, qcName,
                                       studies, contrastName = NULL,
                                       verbose = TRUE, labelSamples = FALSE){
  if(class(miodinDataset) != "MiodinDataset")
    stop("Dataset must be a MiodinDataset")
  if(!(assayName %in% names(getAll(miodinDataset, "assayData"))))
    stop("Assay ", assayName, " does not exist")
  msnSet <- get(miodinDataset, "assayData", name =  assayName)
  if(!is(msnSet, "MSnSet") && !is(msnSet, "ExpressionSet"))
    stop("Assay ", assayName, " is not an MSnSet")
  if(verbose) message("  [INFO] Creating QC report")
  
  #Retrieve contrast vector
  sampleTable <- as(phenoData(msnSet), "data.frame")
  studyName <- get(miodinDataset, "study", assayName)
  contrastVector <- utilMakeContrastVector(studies, studyName,
                                           contrastName, sampleTable)
  
  #Define plotting colors
  plotColors <- utilPlotDefineColors(msnSet, contrastVector)
  if(is.null(plotColors)) return(miodinDataset)
  msnSet <- plotColors$dataset
  samplePheno <- plotColors$samplePheno
  pal <- plotColors$pal
  
  #Sample heatmap
  heatPlot <- call("utilPlotSampleHeatmap", dataset = msnSet,
                   samplePheno = samplePheno, pal = pal)
  
  #Sample PCA
  pcaPlot <- call("utilPlotSamplePCA", dataset = msnSet,
                  samplePheno = samplePheno, pal = pal,
                  labelSamples = labelSamples)
  
  #Add QC plots to dataset
  qcList <- list("qcName" = qcName,  "Sample heatmap" = heatPlot,
                 "PCA" = pcaPlot)
  miodinDataset <- add(miodinDataset, "qualityReport",
                       name = assayName, data = qcList)
  return(miodinDataset)
}

#' @title utilProcessorMassSpecProtein
#' 
#' @description
#' Pre-processing of quantitative proteomics data.
#' 
#' @param miodinDataset \code{MiodinDataset} containing the assay data.
#' @param assayName Name of the assay to process.
#' @param sampleTable Data frame containing sample annotations.
#' @param projectPath Path to the project in which the workflow in executed.
#' @param sampleGroups Factor vector of sample groups.
#' @param contrastName Name of contrast where sample groups are defined.
#' @param filterLowExp Whether or not to filter low-expressed proteins.
#' @param filterLowExpThresh Threshold for low expression.
#' @param filterLowExpNumObs Number of observations that must have expression
#' above \code{filterLowExpThresh} for the protein to be exprssed.
#' @param filterLowVar Whether or not to filter low variation proteins (by CV).
#' @param filterLowVarThresh CV threshold for low variation.
#' @param removeOutliers Whether or not to filter outlier values.
#' @param removeOutliersIQR IQR multiplier used to identify outliers.
#' @param imputeNA Whether or not to impute NAs.
#' @param imputeNATollerance Percentage of replicates that are allowed to be NA
#' for imputation to occur.
#' @param imputeNAMethod Imputation method, must take and return a vector.
#' @param filterNA Whether or not to filter NAs.
#' @param repCollapse Name of replicate collapse column in sample table.
#' @param verbose Whether to print messages.
#' @param moduleName Name of workflow module.
#' @param useBuffer Whether to store processed data in the \code{MiodinDataset}.
#' This gives a memory overhead, but lowers the computational overhead.
#' 
#' @details
#' The following processing steps can be applied to the data:
#' \itemize{
#'   \item Selecting samples in contrast. This is performed based on the factor
#'   vector given to \code{sampleGroups}. Only samples in this vector are
#'   retained in the dataset.
#'   \item Filtering low-expressed proteins. This filters out proteins that are
#'   too low-expressed to be considered in statistical analysis. Proteins are
#'   filtered with
#'   \code{procFilterLowExp}.
#'   \item Filtering low-variance proteins. This filters out proteins that have
#'   too low variance to be considered interesting. Proteins are filtered with
#'   \code{procFilterLowVar}.
#'   \item Replacing outlier expression values with NA. This is done for each
#'   protein and sample group separately to remove extreme values. Outliers
#'   are removed with \code{procRemoveOutliers}.
#'   \item Imputing NAs. This is done to replace any missing values (NA) with
#'   numeric values computed from the non-NA values of the same protein.
#'   Imputation is done for each protein and sample group separately with
#'   \code{procImputeMissingValuesCont}.
#'   \item Filtering proteins with NAs. This is done to remove any proteins
#'   with NA values. Filtering is performed with \code{procFilterMissingValues}.
#'   \item Collapsing replicates by mean. This will collapse all replicates of
#'   a sample into one value. May be used e.g. when technical replicates
#'   should be collapsed prior to analysis. Replicates are collapsed with
#'   \code{procCollapseReplicates}.
#'  }
#' 
#' @return
#' A \code{MiodinDataset} with data processing added as protocol steps.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @export
utilProcessorMassSpecProtein <- function(miodinDataset,
                                         assayName,
                                         sampleTable,
                                         projectPath,
                                         sampleGroups = NULL,
                                         contrastName = NULL,
                                         filterLowExp = FALSE,
                                         filterLowExpThresh = 5,
                                         filterLowExpNumObs = 1,
                                         filterLowVar = FALSE,
                                         filterLowVarThresh = 0.1,
                                         removeOutliers = FALSE,
                                         removeOutliersIQR = 1.5,
                                         imputeNA = FALSE,
                                         imputeNATollerance = 0.1,
                                         imputeNAMethod = mean,
                                         filterNA = FALSE,
                                         repCollapse = NULL,
                                         verbose = TRUE,
                                         moduleName = "",
                                         useBuffer = TRUE){
  if(class(miodinDataset) != "MiodinDataset")
    stop("Dataset must be a MiodinDataset")
  if(!(assayName %in% names(getAll(miodinDataset, "assayData"))))
    stop("Assay ", assayName, " does not exist")
  msnSet <- get(miodinDataset, "assayData", name = assayName)
  if(!is(msnSet, "MSnSet"))
    stop("Assay ", assayName, " is not an MSnSet")
  if(useBuffer)
    miodinDataset <- add(miodinDataset, "processedData", name = assayName,
                         data = msnSet, overwrite = TRUE)
  else
    miodinDataset <- remove(miodinDataset, "processedData", name = assayName)
  
  #Define samplePheno
  if(!is.null(sampleGroups)){
    sampleGroups <- as.factor(sampleGroups)
    sampleGroups <- sort(sampleGroups)
    normProcEnv <- procSelectSamplesInContrast(sampleGroups, contrastName)
    miodinDataset <- add(miodinDataset, what = "processingStep",
                         name = assayName, data = normProcEnv,
                         moduleName = moduleName)
    msnSet <- get(miodinDataset, "assayData", name = assayName)
    samplePheno <- data.frame(Group = sampleGroups)
    colnames(samplePheno) <- contrastName
    rownames(samplePheno) <- colnames(msnSet)
  } else{
    samplePheno <- data.frame(Group = factor(rep(1, ncol(msnSet))))
    rownames(samplePheno) <- colnames(msnSet)
    contrastName <- "Group"
  }
  lvls <- levels(samplePheno[[contrastName]])
  
  #Filter low-expressed
  if(filterLowExp){
    if(verbose) message("  [INFO] Filtering out low-expressed proteins")
    normProcEnv <- procFilterLowExp(filterLowExpThresh, filterLowExpNumObs)
    miodinDataset <- add(miodinDataset, what = "processingStep",
                         name = assayName, data = normProcEnv,
                         moduleName = moduleName)
  }
  
  #Filter low-variation
  if(filterLowVar){
    if(verbose) message("  [INFO] Filtering out low-variance proteins")
    normProcEnv <- procFilterLowVar(filterLowVarThresh)
    miodinDataset <- add(miodinDataset, what = "processingStep",
                         name = assayName, data = normProcEnv,
                         moduleName = moduleName)
  }
  
  #Outlier removal
  if(removeOutliers){
    if(verbose) message("  [INFO] Replacing outliers with NA")
    normProcEnv <- procRemoveOutliers(removeOutliersIQR, samplePheno,
                                      contrastName)
    miodinDataset <- add(miodinDataset, what = "processingStep",
                         name = assayName, data = normProcEnv,
                         moduleName = moduleName)
  }
  
  #Missing value imputation
  if(imputeNA){
    if(verbose) message("  [INFO] Imputing NAs")
    normProcEnv <- procImputeMissingValuesCont(imputeNAMethod,
                                               imputeNATollerance,
                                               samplePheno, contrastName)
    miodinDataset <- add(miodinDataset, what = "processingStep",
                         name = assayName, data = normProcEnv,
                         moduleName = moduleName)
  }
  
  #Filter missing values
  if(filterNA){
    if(verbose) message("  [INFO] Filtering out genes with NA")
    normProcEnv <- procFilterMissingValues()
    miodinDataset <- add(miodinDataset, what = "processingStep",
                         name = assayName, data = normProcEnv,
                         moduleName = moduleName)
  }
  
  #Collapse replicates
  if(!is.null(repCollapse)){
    if(verbose) message("  [INFO] Collapsing replicates by mean")
    for(repCol in repCollapse){
      msnSet <- get(miodinDataset, "assayData", name = assayName)
      sampleTable <- as(phenoData(msnSet), "data.frame")
      if(is.na(match(repCol, colnames(sampleTable))))
        stop("Replicate collapse columns ", repCol,
             " not found in sample table")
      repColFactor <- factor(sampleTable[[repCol]])
      
      #For each replicate level, take mean of replicates
      normProcEnv <- procCollapseReplicates(sampleTable, repCol, repColFactor)
      miodinDataset <- add(miodinDataset, what = "processingStep",
                           name = assayName, data = normProcEnv,
                           moduleName = moduleName)
    }
  }
  
  return(miodinDataset)
}
