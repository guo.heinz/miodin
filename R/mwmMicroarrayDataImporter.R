#Include MicroarrayDataImporter dependencies
#' @include standardGenerics.R MiodinWorkflowModule.R utilsMicroarray.R
NULL

#' @title MicroarrayDataImporter
#' 
#' @description
#' Workflow module class for importing raw microarray data.
#' 
#' @details
#' 
#' The module allows raw RNA, SNP and methylation microarray data to be imported
#' into a \code{MiodinDataset}. Supported arrays are listed with
#' \code{chipAnnotations()}.
#' 
#' @section Input ports:
#' 
#' The module has not input ports.
#' 
#' @section Output ports:
#' 
#' The module has a single output port:
#' \itemize{
#'   \item \code{dataset}: This contains two slots, \code{mds} and
#'   \code{assayName}, which contain the \code{MiodinDataset} and imported assay
#'   name, respectively.
#' }
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{MiodinWorkflowModule-class}
#' 
#' @name WorkflowModule-MicroarrayDataImporter
#' @export MicroarrayDataImporter
MicroarrayDataImporter <- setClass(
  "MicroarrayDataImporter",
  prototype = list(
    parameterValues = list(
      platform = "",
      dataType = "",
      studyName = "",
      assayName = "",
      datasetName = "",
      contrastName = NULL,
      chipName = "",
      probeAnnotationFile = NULL,
      probeSummarization = "transcript",
      filterFailedProbes = TRUE,
      filterFailedProbesThresh = 0.05,
      filterFailedProbesNumObs = 1,
      technicalRep = "",
      labelSamples = FALSE,
      excludeSamples = NULL
    ),
    documentation = "",
    instantiator = "importMicroarrayData"
  ),
  validity = function(object){
    errors <- character()
    if(length(errors) == 0)
      return(TRUE)
    else
      return(errors)
  },
  contains = "MiodinWorkflowModule",
  sealed = TRUE
)

#' @export
setMethod("initialize", "MicroarrayDataImporter",
  function(.Object, ...){
    .Object <- .Object +
      outputDataPort(
        name = "dataset",
        slots = c(mds = "MiodinDataset", assayName = "character"),
        slotDesc = c(mds = "MiodinDataset containing assay data",
                     assayName = "Names of assays to analyze")
      )
    .Object <- setProp(.Object, list(
      parameterDesc = list(Assay = data.frame(
        Parameter = c("platform", "dataType", "studyName",
                      "assayName", "datasetName", "chipName",
                      "contrastName", "probeAnnotationFile",
                      "probeSummarization", "filterFailedProbes",
                      "filterFailedProbesThresh",
                      "filterFailedProbesNumObs"),
        Data = c("Character", "Character", "Character", "Character",
                 "Character", "Character", "Character", "Character",
                 "Character", "Logical", "Numeric", "Numeric"),
        Mandatory = c("Yes", "Yes", "Yes", "Yes", "Yes", "Yes", "No", "No",
                      "No", "No", "No", "No"),
        Description = c(
          "Experimental platform (affymetrix or illumina)",
          "Omics data type (rna, snp or methylation)",
          "Name of the study in which assay is defined",
          "Name of assay to import",
          "Name of output dataset",
          "Name of microarray platform, see chipAnnotations()",
          "Name of contrast in study design used to color sample groups",
          "Path to custom probe annotation file",
          "Either 'transcript' (transcript cluster summarization), or 'probeset' (probeset summarization)",
          "Whether or not to remove failed methylation probes",
          "Detection p-value below which methylation probes are considered failed",
          "Number of observations with failed probes required to remove the probe"))
      )
    ))
    return(.Object)
  } )

setMethod("execute", "MicroarrayDataImporter",
  function(object, studies, projectPath, workflowConfiguration,
           inputPorts, platform, dataType, studyName, assayName,
           datasetName, contrastName, chipName, probeAnnotationFile,
           probeSummarization, filterFailedProbes,
           filterFailedProbesThresh, filterFailedProbesNumObs,
           technicalRep, labelSamples, excludeSamples, verbose, moduleName){
    
    #Validate arguments
    if(!(platform %in% c("affymetrix", "illumina")))
      stop("platform must be either affymetrix or illumina")
    if(!(dataType %in% c("rna", "snp", "methylation")))
      stop("dataType must be either rna, snp or methylation")
    if(studyName == "" || !is.character(studyName))
      stop("studyName must be a non-empty character string")
    if(!(studyName %in% names(studies)))
      stop("Study ", studyName, " does not exist")
    if(assayName == "" || !is.character(assayName))
      stop("assayName must be a non-empty character string")
    if(!(assayName %in% names(getAll(studies[[studyName]], "assayTable"))))
      stop("Assay table ", assayName, " does not exist")
    if(datasetName == "" || !is.character(datasetName))
      stop("datasetName must be a non-empty character string")
    if(!is.logical(labelSamples))
      stop("labelSamples must be TRUE or FALSE")
    
    #Retrieve sample table
    sampleTable <- getProp(get(studies[[studyName]],
                               "sampleTable"), "table")[["table"]]

    #Retrieve assay table
    assayTable <- get(studies[[studyName]], "assayTable", assayName)
    assayTable <- getProp(assayTable, "table")[["table"]]
    if(!("DataFile" %in% colnames(assayTable)))
      stop("Assay table ", assayName, " is missing the FileName column")
    if(!("SampleName" %in% colnames(assayTable)))
      stop("Assay table ", assayName, " is missing the SampleName column")
    samplesInc <- which(!assayTable$DataFile %in% c("", NA))
    assayTable <- assayTable[samplesInc, , drop = FALSE]
    sampleIncNames <- unique(assayTable$SampleName)
    if(any(is.na(match(sampleIncNames, sampleTable$SampleName))))
      stop("Sample names in assay table are missing in the sample table")
    sampleTable <- sampleTable[match(assayTable$SampleName,
                                     sampleTable$SampleName), , drop = FALSE]
    if(nrow(assayTable) == 0)
      stop("No assay files to import")
    if(technicalRep != "" && !technicalRep %in% colnames(sampleTable))
      stop("Technical replicate column ", technicalRep,
           " not found in sample table")
    
    #Platform and data-specific processing
    if(platform == "affymetrix" && dataType == "rna"){
      miodinDataset <- utilImporterAffymetrixRNA(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        chipName = chipName,
        probeSummarization = probeSummarization,
        probeAnnotationFile = probeAnnotationFile,
        workflowConfiguration = workflowConfiguration,
        projectPath = projectPath,
        excludeSamples = excludeSamples,
        verbose = verbose,
        moduleName = moduleName)
      miodinDataset <- utilQualityMicroarrayRNA(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Normalized data",
        contrastName = contrastName,
        studies = studies,
        technicalRep = technicalRep,
        verbose = verbose,
        labelSamples = labelSamples)
    } else if(platform == "affymetrix" && dataType == "snp"){
      miodinDataset <- utilImporterAffymetrixSNP(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        chipName = chipName, 
        projectPath = projectPath,
        probeAnnotationFile = probeAnnotationFile,
        workflowConfiguration = workflowConfiguration,
        excludeSamples = excludeSamples,
        verbose = verbose,
        moduleName = moduleName)
      miodinDataset <- utilQualityMicroarraySNP(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Normalized data",
        studies = studies,
        projectPath = projectPath,
        contrastName = contrastName,
        verbose = verbose,
        labelSamples = labelSamples)
    } else if(platform == "illumina" && dataType == "rna"){
      miodinDataset <- utilImporterIlluminaRNA(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        chipName = chipName,
        probeAnnotationFile = probeAnnotationFile,
        projectPath = projectPath,
        excludeSamples = excludeSamples,
        verbose = verbose,
        moduleName = moduleName)
      miodinDataset <- utilQualityMicroarrayRNA(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Normalized data",
        contrastName = contrastName,
        studies = studies,
        technicalRep = technicalRep,
        verbose = verbose,
        labelSamples = labelSamples)
    } else if(platform == "illumina" && dataType == "snp"){
      miodinDataset <- utilImporterIlluminaSNP(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        chipName = chipName,
        projectPath = projectPath,
        probeAnnotationFile = probeAnnotationFile,
        excludeSamples = excludeSamples,
        verbose = verbose,
        moduleName = moduleName)
      miodinDataset <- utilQualityMicroarraySNP(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Normalized data",
        studies = studies,
        projectPath = projectPath,
        contrastName = contrastName,
        verbose = verbose,
        labelSamples = labelSamples)
    } else if(platform == "illumina" && dataType == "methylation"){
      miodinDataset <- utilImporterIlluminaMethyl(
        datasetName = datasetName,
        assayTable = assayTable,
        assayName = assayName,
        studyName = studyName,
        sampleTable = sampleTable,
        contrastName = contrastName,
        studies = studies,
        probeAnnotationFile = probeAnnotationFile,
        projectPath = projectPath,
        filterFailedProbes = filterFailedProbes,
        filterFailedProbesThresh = filterFailedProbesThresh,
        filterFailedProbesNumObs = filterFailedProbesNumObs,
        chipName = chipName,
        excludeSamples = excludeSamples,
        verbose = verbose,
        moduleName = moduleName)
      miodinDataset <- utilQualityMicroarrayMethyl(
        miodinDataset = miodinDataset,
        assayName = assayName,
        qcName = "Normalized data",
        contrastName = contrastName,
        studies = studies, qcType = "assay",
        verbose = verbose,
        labelSamples = labelSamples)
    }

    #Return output ports
    return(list(
      dataset = list(
        mds = miodinDataset, assayName = assayName
      )
    ))
  } )

#' @title importMicroarrayData
#' 
#' @description
#' Workflow module for importing microarray data.
#' 
#' @param name Name of the workflow module (character string).
#' @param platform Experimental platform (\code{"affymetrix"} or
#' \code{"illumina"}).
#' @param dataType Omics data type (rna, snp or methylation).
#' @param studyName Name of the study in which assay is defined.
#' @param assayName Name of assay to import.
#' @param datasetName Name of output dataset.
#' @param chipName Name of microarray platform, see chipAnnotations().
#' @param contrastName Name of contrast in study design used to color sample
#' groups.
#' @param probeAnnotationFile Path to custom probe annotation file.
#' @param probeSummarization Either 'transcript' (transcript cluster
#' summarization), or 'probeset' (probeset summarization).
#' @param technicalRep Sample table column contain information about technical
#' replicates.
#' @param filterFailedProbes Whether or not to remove failed methylation probes.
#' @param filterFailedProbesThresh Detection p-value below which methylation
#' probes are considered failed.
#' @param filterFailedProbesNumObs Number of observations with failed probes
#' required to remove the methylation probe.
#' @param labelSamples Whether or not to label samples in the QC PCA plot.
#' @param excludeSamples Character vector of samples to exclude.
#' 
#' @details
#' This module is used to import raw microarray data from Affymetrix arrays
#' (CEL-files) and Illumina arrays (IDAT-files). Omics data types supported
#' include transcriptomics, genomics (SNP) and methylation data. The platform
#' of the arrays to import is specified with \code{platform} and the data type
#' with \code{dataType}. The type of microarray chip is specified with
#' \code{chipName}. To see supported chip types, use
#' \code{rownames(chipAnnotations())}.
#' 
#' Additional arguments that need to be specified include the name of the study
#' in which the assays are defined (\code{studyName}), name of the assay table
#' in the study (\code{assayName}) and name of the output dataset
#' (\code{datasetName}). The name of a contrast in the study may also be
#' specified with \code{contrastName} in order to color-code samples according
#' to which group they belong in quality control (QC) plots generated. Another
#' optional argument is \code{technicalRep}, which can be set to a sample table
#' column name. The column should contain information about which samples are
#' technical replicates, and this will be used to generate a technical replicate
#' QC plot. Information about probe annotation and steps specific to each
#' data type is given in the sections below.
#' 
#' @section Probe annotation:
#' Imported data is automatically annotated.
#' By default, official array annotation is downloaded from the Affymetrix or
#' Illumina server. The user many also supply a custom annotation file with
#' \code{probeAnnotationFile} to use instead. The point of annotating the probes
#' is to know to which transcript/SNP/CpG site the probes hybridize to.
#' 
#' In order to annotate Affymetrix data, it is necessary to provide login
#' credentials to the Affymetrix server. This can be obtained online by visiting
#' \url{http://www.affymetrix.com/analysis/index.affx} and clicking Register at
#' the top of the page. The credentials need to be added to the workflow
#' configuration like so.
#' 
#' \preformatted{
#' mw <- MiodinWorkflow("AnalysisFlow")
#' mw <- mw + workflowConfiguration(
#'   affyUser = "username",
#'   affyPass = "passwd")
#' }
#' 
#' In order to not save the password as text in the script, the \code{getPass}
#' package can be used.
#' 
#' \preformatted{
#' mw <- MiodinWorkflow("AnalysisFlow")
#' mw <- mw + workflowConfiguration(
#'   affyUser = "username",
#'   affyPass = getPass::getPass("Password"))
#' }
#' 
#' @section Importing transcriptomics data:
#' 
#' Transcriptomics data from Affymetrix arrays is imported with the \code{oligo}
#' package and normalized with the Robust Multichip Average (RMA) method
#' (Carvalho et al. 2010). For Gene and Exon type arrays it is necessary to
#' decide whether
#' to summarize the raw probe signals on transcript or probeset. This is
#' specified with \code{probeSummarization} and defaults to transcript. This
#' implies that the rows of the expression data will correspond to different
#' transcripts. With probeset summarization, the rows will correspond to
#' individual exons of transcripts instead.
#' 
#' Transcriptomics data from Illumina arrays is imported with the \code{limma}
#' package and normalized with the quantile normalization method
#' (Ritchie et al. 2015). Here it is not necessary to set
#' \code{probeSummarization}.
#' 
#' Quality control plots are generated for normalized data and include a plot to
#' inspect data distribution,
#' MA-plots for inspecting intensity-dependent bias, sample heatmap to inspect
#' sample clustering and a sample PCA plot to inspect sample clustering, batch
#' effects and outliers.
#' 
#' @section Importing genomics data:
#' 
#' Affymetrix and Illumina SNP array data is imported, normalized and
#' genotyped with the CRLMM method in the \code{crlmm} package
#' (Carvalho et al. 2009). Quality control plots generated for genotyped data
#' and include a plot
#' to inspect confidence of genotype calls, a sample call rate plot and a
#' sample heterozygosity plot (to inspect sample quality), and a sample PCA
#' and sample heatmap (to inspect clustering, batch effects and outliers).
#' 
#' @section Importing methylation data:
#' 
#' Illumina methylation array data can be imported from 450K and EPIC arrays.
#' Data is imported, normalized with quantile normalization and mapped to the
#' human genome with the \code{minfi} package (Fortin et al. 2016). There is an
#' option to filter out failed CpG probes by setting
#' \code{filterFailedProbes=TRUE}. Probes are consdiered failed if their
#' detection p-value is above \code{filterFailedProbesThresh} (defaults to 0.05)
#' in at least \code{filterFailedProbesNumObs} number of samples (defaults to
#' 1).
#' 
#' Quality control plots are generated for both raw and normalized data. Raw
#' QC plots include a detection p-values plot, a plot with the number of failed
#' probes and a control probes plot. Normalized data QC include density plots
#' for Beta- and M-values to inspect normalization, a sample heatmap to inspect
#' clustering and a sample PCA plot to inspect sample clustering, batch
#' effects and outliers.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{WorkflowModule-MicroarrayDataImporter}
#' 
#' @references
#' Carvalho, B. S., Louis, T. A., & Irizarry, R. A. (2009). Quantifying
#' uncertainty in genotype calls. Bioinformatics, 26(2), 242-249.
#' 
#' Carvalho, B. S., & Irizarry, R. A. (2010). A framework for
#' oligonucleotide microarray preprocessing. Bioinformatics, 26(19), 2363-2367.
#' 
#' Fortin, J. P., Triche Jr, T. J., & Hansen, K. D. (2016).
#' Preprocessing, normalization and integration of the Illumina
#' HumanMethylationEPIC array with minfi. Bioinformatics, 33(4), 558-560.
#' 
#' Ritchie, M. E., Phipson, B., Wu, D., Hu, Y., Law, C. W., Shi,
#' W., & Smyth, G. K. (2015). limma powers differential expression
#' analyses for RNA-sequencing and microarray studies. Nucleic acids
#' research, 43(7), e47-e47.
#' 
#' @name InstatiateModule-importMicroarrayData
#' @export importMicroarrayData
importMicroarrayData <- function(name,
                                 platform = "",
                                 dataType = "",
                                 studyName = "",
                                 assayName = "",
                                 datasetName = "",
                                 contrastName = NULL,
                                 chipName = "",
                                 probeAnnotationFile = NULL,
                                 probeSummarization = "transcript",
                                 technicalRep = "",
                                 filterFailedProbes = TRUE,
                                 filterFailedProbesThresh = 0.05,
                                 filterFailedProbesNumObs = 1,
                                 labelSamples = FALSE,
                                 excludeSamples = NULL){
  mwm <- workflowModule("MicroarrayDataImporter", name,
                        platform = platform,
                        dataType = dataType,
                        studyName = studyName,
                        assayName = assayName,
                        datasetName = datasetName,
                        contrastName = contrastName,
                        chipName = chipName,
                        probeAnnotationFile = probeAnnotationFile,
                        probeSummarization = probeSummarization,
                        technicalRep = technicalRep,
                        filterFailedProbes = filterFailedProbes,
                        filterFailedProbesThresh = filterFailedProbesThresh,
                        filterFailedProbesNumObs = filterFailedProbesNumObs,
                        labelSamples = labelSamples,
                        excludeSamples = excludeSamples)
  return(mwm)
}
