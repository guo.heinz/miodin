.onAttach <- function(libname, pkgname) {
  packageStartupMessage("Welcome to Miodin: An R package for",
                        " multi-omics data integration")
}

.onLoad <- function(libname, pkgname) {
  op <- options()
  op.miodin <- list(
    miodin.defop = op
  )
  toset <- !(names(op.miodin) %in% names(op))
  if(any(toset)) options(op.miodin[toset])
  invisible()
}

.onUnload <- function(libname){
  options(getOption("miodin.defop"))
	invisible()
}
