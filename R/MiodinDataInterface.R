#Include MiodinDataInterface dependencies
#' @include standardGenerics.R MiodinWorkflowModule.R MiodinDataPort.R
NULL

#' @title MiodinDataInterface
#' 
#' @description
#' The \code{MiodinDataInterface} class represents a data transfer connection
#' between the output port of one workflow module and the input port of another.
#' 
#' \preformatted{## Class constructor
#' MiodinDataInterface()}
#' 
#' @param object A \code{MiodinDataInterface} object.
#' @param props Named list of properties to set (for \code{setProp}) or a
#' character vector (for \code{getProp}).
#' @param res Named used to return parameters.
#' 
#' @details
#' 
#' When a workflow is created, \code{MiodinDataInterface}s are used to map the
#' output of workflow modules to the input of other modules. Modules define
#' the output they promise to deliver through \code{MiodinDataPort} output ports
#' and the input they require through \code{MiodinDataPort} input ports. To
#' transfer data from an output port to an input port, the modules are connected
#' by a \code{MiodinDataInterface}, which ensures that the ports are compatible
#' and that the correct output port slot is received by the corresponding input
#' port slot.
#' 
#' @section Properties of \code{MiodinDataInterface}:
#' 
#' The following properties can be set/retrieved with
#' \code{setProp}/\code{getProp}.
#' \itemize{
#'   \item \code{"connection"}: The elements of a \code{MiodinDataInterface}
#'   object. Must have names \code{"outputModule"}, \code{"outputPort"},
#'   \code{"outputSlots"}, \code{"inputModule"}, \code{"inputPort"},
#'   \code{"inputSlots"}. The names \code{"outputWorkflow"} and
#'   \code{"inputWorkflow"} can also be set.
#'   \item \code{"inputModule"}: The name of the module accepting the interface
#'   as input.
#'   \item \code{"outputModule"}: The name of the module from which output data
#'   is provided.
#' }
#'
#' @slot outputWorkflow The name of the workflow in which the output module
#' is located.
#' @slot outputModule Name of the \code{MiodinWorkflowModule} holding the
#' output port.
#' @slot outputPort \code{MiodinDataPort} output port of the output module.
#' @slot outputPortName Name of output \code{MiodinDataPort}.
#' @slot outputSlots Character vector of slot names in the output port.
#' @slot inputWorkflow The name of the workflow in which the input module
#' is located.
#' @slot inputModule Name of the \code{MiodinWorkflowModule} holding the
#' input port.
#' @slot inputPort \code{MiodinDataPort} input port of the input module.
#' @slot inputPortName Name of input \code{MiodinDataPort}.
#' @slot inputSlots Character vector of slot names in the input port.
#' 
#' @author Benjamin Ulfenborg
#' 
#' @seealso \link{MiodinWorkflow-class} \link{MiodinWorkflowModule-class}
#' \link{MiodinWorkflowModuleRegistry-class} \link{MiodinDataPort-class}
#' 
#' @name MiodinDataInterface-class
#' @export MiodinDataInterface
MiodinDataInterface <- setClass(
  "MiodinDataInterface",
  slots = list(
    outputWorkflow = "character",
    outputModule = "character",
    outputPort = "MiodinDataPort",
    outputPortName = "character",
    outputSlots = "character",
    inputWorkflow = "character",
    inputModule = "character",
    inputPort = "MiodinDataPort",
    inputPortName = "character",
    inputSlots = "character"
  ),
  prototype = list(
    outputSlots = c(),
    inputSlots = c()
  ),
  validity = function(object){
    errors <- character()
    if(length(errors) == 0)
      return(TRUE)
    else
      return(errors)
  },
  sealed = TRUE
)

#' @describeIn MiodinDataInterface Sets properties of the interface.
setMethod("setProp", "MiodinDataInterface",
  function(object, props){
    if(!class(props)[1] == "list") stop("props must be a list")
    if(length(props) == 0) stop("props must not be empty")
    if(is.null(names(props))) stop("props must be a named list")
    for(prop in names(props)){
      if(!(prop %in% c("connection", "inputModule", "outputModule")))
        stop("Unknown interface property ", prop)
      value <- props[[prop]]
      if(prop == "connection"){
        if(!is.list(value))
          stop("Connection must be a list")
        if(is.null(names(value)))
          stop("Connection must be a named list")
        if(any(!(c("outputModule", "outputPort", "outputPortName",
                   "outputSlots", "inputModule", "inputPort", "inputPortName",
                   "inputSlots") %in% names(value))))
          stop("Connection must specify outputModule, outputPort,
               outputPortName, outputSlots, ",
               "inputModule, inputPort, inputPortName, inputSlots")
        oSlotClasses <- getProp(value$outputPort,
                                "slots")[["slots"]][value$outputSlots]
        iSlotClasses <- getProp(value$inputPort,
                                "slots")[["slots"]][value$inputSlots]
        ind <- which(unlist(oSlotClasses) != unlist(iSlotClasses))
        if(length(ind) > 0)
          stop("Class mismatch beween input slots (",
               paste0(names(iSlotClasses)[ind], collapse = ", "),
               ") and output slots (",
               paste0(names(oSlotClasses)[ind], collapse = ", "), "): ",
               paste0(paste(iSlotClasses[ind], oSlotClasses[ind], sep = "-"),
                      collapse = ", "))
        object@outputWorkflow <- value$outputWorkflow
        object@outputModule <- value$outputModule
        object@outputPort <- value$outputPort
        object@outputPortName <- value$outputPortName
        object@outputSlots <- value$outputSlots
        object@inputWorkflow <- value$inputWorkflow
        object@inputModule <- value$inputModule
        object@inputPort <- value$inputPort
        object@inputPortName <- value$inputPortName
        object@inputSlots <- value$inputSlots
        if("outputWorkflow" %in% names(value))
          object@outputWorkflow <- value$outputWorkflow
        if("inputWorkflow" %in% names(value))
          object@inputWorkflow <- value$inputWorkflow
      }
      if(prop == "inputModule"){
        if(!is.character(value))
          stop("inputModule must be character")
        object@inputModule <- value
      }
      if(prop == "outputModule"){
        if(!is.character(value))
          stop("outputModule must be character")
        object@outputModule <- value
      }
    }
    return(object)
  } )

#' @describeIn MiodinDataInterface Returns properties of the interface.
setMethod("getProp", "MiodinDataInterface",
  function(object, props, res = list()){
    if(length(props) == 0)
      stop("props must not be empty")
    if(!is.character(props))
      stop("props must be a character vector")
    for(prop in props){
      if(!(prop %in% c("connection", "inputModule", "outputModule")))
        stop("Unknown interface property ", prop)
      if(prop == "connection")
        res[["connection"]] <- list(
          outputWorkflow = object@outputWorkflow,
          outputModule = object@outputModule,
          outputPort = object@outputPort,
          outputPortName = object@outputPortName,
          outputSlots = object@outputSlots,
          inputWorkflow = object@inputWorkflow,
          inputModule = object@inputModule,
          inputPort = object@inputPort,
          inputPortName = object@inputPortName,
          inputSlots = object@inputSlots
        )
      if(prop == "inputModule")
        res[["inputModule"]] <- object@inputModule
      if(prop == "outputModule")
        res[["outputModule"]] <- object@outputModule
    }
    return(res)
  } )
